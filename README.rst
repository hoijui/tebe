============================
Tebe - rst and sphinx writer
============================
Tebe is a simple but powerful editor for Markdown and reStructuredText markup languages
with Sphinx and Rst2Pdf power included.
It can be used as a text editor for creating articles or even books.
You can also use it to work with any sphinx docs you have for yours project.
The mission of the project is to provide a simple and practical tool that will interest non-programer people
to use markup syntax and sphinx for any text document writing.

Changelog
---------

Tebe 0.1

- first public release (beta stage) with all main features implemented

Requirements
------------
1. Python 2.7
#. pyqt4 or PySide
#. sphinx
#. rst2pdf
#. docutils
#. recommonmark
#. mistune

How to install these on Debian/Ubuntu Linux::

  sudo apt-get install python-pip
  pip install PySide sphinx rst2pdf docutils recommonmark mistune

How to install
--------------
Tebe is available through PyPI and can be install with pip command.
To install Tebe use pip by typing::

   pip install tebe

Please note that the requirements listed above must be installed separately.

To run tebe, just run the ``Tebe.py`` file from the installed package directory.

On Linux, you would run Tebe like this::

  python2 "$HOME/.local/lib/python2"*"/site-packages/tebe/Tebe.py"

Please find step by step install instruction on the ``project website``_.

.. _``project website``: https://tebe.readthedocs.io

Licence
-------
Tebe is free software;
you can redistribute it and/or modify it under the terms of the **GNU General Public License**
as published by the Free Software Foundation;
either version 2 of the License,
or (at your option) any later version.

Copyright (C) 2017-2018 Lukasz Laba <lukaszlab@o2.pl>

Contributions
-------------
If you want to help out, create a pull request or write email.

More information
----------------
Project website: https://tebe.readthedocs.io

Code repository: https://bitbucket.org/lukaszlaba/tebe

PyPI package: https://pypi.python.org/pypi/tebe

Contact: Lukasz Laba <lukaszlab@o2.pl>
