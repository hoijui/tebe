import os
import add_tebe_to_sys_path
from example_sphinx_dir_content_path import example_sphinx_dir_content_path

from tebe.pycore.Content import Content
from tebe.pycore.Rst2PdfBuilder import Rst2PdfBuilder

Rst2PdfBuilder = Rst2PdfBuilder()

some_rst_path = os.path.join(example_sphinx_dir_content_path, 'features.rst')

Rst2PdfBuilder.build_pdf_from_rst_file(some_rst_path)